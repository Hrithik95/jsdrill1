const carArr = require('./cars.cjs');

// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of: 
//"Car 33 is a *car year goes here* *car make goes here* *car model goes here*"

function problem1(arr=[],searchId){
    if(!Array.isArray(arr) || arr.length == 0 || typeof searchId == "undefined"){
        return [];
    }
    for(let index=0;index<arr.length;index++){
        if(arr[index]['id'] == searchId){
             const result = [];
             result.push(arr[index]);
             return result;
        }
    }
    return [];
}

// let ans = problem1(carArr);
module.exports = problem1;