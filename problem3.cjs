const carArr = require('./cars.cjs');

// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.
function problem3(arr){
    const car_models = [];
    for(let index=0;index<arr.length;index++){
        car_models.push(arr[index]['car_model']);
    }
    car_models.sort();
    for(let index=0;index<car_models.length;index++){
        console.log(car_models[index]);
    }
}


module.exports = problem3;