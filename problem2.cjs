const carArr = require('./cars.cjs');


// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of: 
//"Last car is a *car make goes here* *car model goes here*"

function problem2(arr){
    if(arr.length == 0 || typeof arr == "undefined"){
        return "No inventory available";
    }
    return ("Last car is a " + arr[arr.length-1]['car_make'] + " " + arr[arr.length-1]['car_model']);
}

module.exports = problem2;