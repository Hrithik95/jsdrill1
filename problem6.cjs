// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

const carArr = require('./cars.cjs');

function problem6(arr){
    const BMWAndAudi = [];
    for(let index=0;index<arr.length;index++){
        if(arr[index]['car_make'] == 'Audi' || arr[index]['car_make'] == 'BMW'){
            BMWAndAudi.push(arr[index]);
        }
    }
    return BMWAndAudi;
}

// problem6(carArr);
module.exports = problem6;
