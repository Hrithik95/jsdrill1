// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.
const carArr = require('./cars.cjs');

function problem4(arr){
    let car_years = [];
    for(let index=0;index<arr.length;index++){
        car_years.push(arr[index]['car_year']);
    }
    // console.log(car_years);
    return car_years;
}

// let ans = problem4(carArr);

module.exports = problem4;