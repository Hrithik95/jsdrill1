// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

const car_years = require('./problem4.cjs');

function problem5(arr){
    let carOlderThan2000 = [];
    for(let index=0;index<arr.length;index++){
        if(arr[index] < 2000){
            carOlderThan2000.push(arr[index]);
        }
    }
    // console.log(carOlderThan2000);
    // console.log(carOlderThan2000.length);
    return carOlderThan2000;
}

// problem5(car_years);
module.exports = problem5;