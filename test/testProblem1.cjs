const carId33 = require('../problem1.cjs');

const carArr = require('../cars.cjs');

// console.log(carId33(carArr));

let ans = carId33({id: 33, name: "Test", length: 10}, 33);
if(ans.length == 0){
    console.log("No array present");
    return;
}
// if(typeof ans == "string"){
//     console.log(ans);
//     return;
// }
// console.log(ans);
console.log("Car " + ans[0]['id'] + " is a " + ans[0]['car_year'] + " " + ans[0]['car_make'] + " " + ans[0]['car_model'])